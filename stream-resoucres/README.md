# Streaming Resources
`session.svg` : Template for sessions \[Talk Title, Speaker, Date & Time and Room\].

`runner.svg` : Source file of the images to be run on the stream until the conference.

Font Used: [Quicksand](https://fonts.google.com/specimen/Quicksand)
